## coreos-install

Drop-in /var/lib/coreos-install for CoreOS systems with an update mechanism.

Aimed at small collections of one or more *non-clustered* servers where same
configuration would be nice but there's no particular reason they should be
talking to each other. There's no reason it shouldn't work on a cluster, but
then surely you have better configuration management than cloning a git repo
onto every machine?

#### Single-system software

coreos-install runs on a single system by design. It's not syncing systems
*together*, or keeping other systems in sync with itself, it just tries to
update its own system to a central repository. You can probably do some neat
git wrangling to decentralise it if you wanted (offhand, you could probably
have a few servers publishing their own local repos and a cron job to pull
changes from each other), but then you might as well cluster them.

This is really just poor-man's configuration management.

### Installation

    $ cd /var/lib
    $ sudo git clone https://github.com/muhmuhten/coreos-install.git
    $ sudo reboot

Of course, you should clone your own forks of the repositories.

This will literally execute arbitrary code downloaded from the internet, so
keep the implications of that in mind.
